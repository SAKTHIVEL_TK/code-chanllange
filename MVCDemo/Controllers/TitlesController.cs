﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MVCDemo.Models;

namespace MVCDemo.Controllers
{
    public class TitlesController : Controller
    {               
        private TitlesEntities _titlesEntities;
        List<TitlesModel> viewModel;
        public TitlesModel titleModel;

        public TitlesController()
        {
            _titlesEntities = new TitlesEntities();
        }

        //
        // GET: /Home/
        public ActionResult Index()
        {            
            return View();
        }

        //
        // GET: /Titles/ReadTitles
        public ActionResult ReadTitles()
        {
            ViewBag.TitleInfo = _titlesEntities.Titles.ToList();                        
            return View();
        }

        //
        // GET: /Titles/GetTitles
        [HttpGet]
        public ActionResult GetTitles(int titleId)
        {
            viewModel = new List<TitlesModel>();
            var query = (from T in _titlesEntities.Titles
                         join S in _titlesEntities.StoryLines on T.TitleId equals S.TitleId                         
                         join A in _titlesEntities.Awards on S.TitleId equals A.TitleId
                         join Tp in _titlesEntities.TitleParticipants on T.TitleId equals Tp.TitleId
                         join P in _titlesEntities.Participants on Tp.Id equals P.Id
                         where T.TitleId == titleId
                         select new
                         {
                             Title = T.TitleName,
                             ReleaseYear = T.ReleaseYear,
                             Language = S.Language,
                             Type=S.Type,                             
                             Awards = A.Award1,
                             ParticipantName = P.Name                                                                                        
                         }).ToList().Distinct();

            foreach (var q in query)
            {
                titleModel = new TitlesModel();
                titleModel.Title=q.Title.ToString();
                titleModel.ReleaseYear=q.ReleaseYear.ToString();
                titleModel.Language=q.Language.ToString();                
                titleModel.Type = q.Type.ToString();                
                titleModel.AwardName = q.Awards.ToString();
                titleModel.ParticipantName = q.ParticipantName.ToString();                
                viewModel.Add(titleModel); 
            }            

            List<TitlesModel> vModel = new List<TitlesModel>();

            foreach(TitlesModel item in viewModel)
            {
                if (vModel.Contains(item))
                { break;}
                else { vModel.Add(item); }
            }                           
            ViewBag.Details = vModel;                        
            return View("_GetTitles");
        }                       
    }
}

