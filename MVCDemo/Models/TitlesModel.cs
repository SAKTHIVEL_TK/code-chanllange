﻿namespace MVCDemo.Models
{
    public class TitlesModel
    {
        public string Title { get; set; }
        public string ReleaseYear { get; set; }
        public string Language { get; set; }
        public string Descr { get; set; }
        public string Type { get; set; }
        public string GenreName { get; set; }
        public string AwardName { get; set; }
        public string ParticipantId { get; set; }
        public string ParticipantName { get; set; }
    }    
}
